output "certificate_aws" {
  value = "${aws_acm_certificate.domain.arn}"
}