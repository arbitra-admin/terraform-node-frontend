variable "env" {}
variable "certificate_arn" {}
variable "domain" {}
variable "zone_id" {}
variable "bucket_name" {
    default = "tf-boilerplate-frontend"
}
variable "origin_id" {
    default = "tf-boilerplate"
}



