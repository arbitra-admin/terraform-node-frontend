REPOSITORY_URL=${AWS_ACCOUNT_ID}.dkr.ecr.ap-southeast-2.amazonaws.com/tf-ci

aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.ap-southeast-2.amazonaws.com/tf-ci

eval $(aws ecr get-login --no-include-email --region ap-southeast-2)

docker build -t $REPOSITORY_URL . 
docker tag ${AWS_ACCOUNT_ID}.dkr.ecr.ap-southeast-2.amazonaws.com/tf-ci ${AWS_ACCOUNT_ID}.dkr.ecr.ap-southeast-2.amazonaws.com/tf-ci:latest
docker push ${AWS_ACCOUNT_ID}.dkr.ecr.ap-southeast-2.amazonaws.com/tf-ci:latest