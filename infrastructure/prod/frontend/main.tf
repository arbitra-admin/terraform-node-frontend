terraform {
  backend "s3" {}
}

data "terraform_remote_state" "route" {
    backend = "s3"
    config {
        key="FRONTEND/uno-elastic-frontend-deployment/infrastructure/global/route/route.tfstate"
        bucket="uno-terraform-remote-state"
        region="ap-southeast-2"
    }
}

module "frontend" {
  source = "../../modules/certificate"

  domain = "${data.terraform_remote_state.route.domain}"
  zone_id = "${data.terraform_remote_state.route.zone_id}"
  
}

module "cloudfront" {
  source = "../../modules/cloudfront"

  env = "prod"
  certificate_arn = "${module.frontend.certificate_aws}"
  domain = "${data.terraform_remote_state.route.domain}"
  zone_id = "${data.terraform_remote_state.route.zone_id}"
}
