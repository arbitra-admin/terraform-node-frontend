terraform {
  backend "s3" {}
}

resource "aws_route53_zone" "primary" {
    name = "${var.domain}"
    comment = "Primary hosted zone for uno's boilerplate integration"
}
