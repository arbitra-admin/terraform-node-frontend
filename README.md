# PURPOSE #

This is a comperehensive guide on how to standardise the deployment of Front End resources written in Javascript which uses AWS Resources such as S3 Bucket, CloudFront Distribution, Route53 and AWS Certificate Manager. The main differentiator of this deployment boilerplate is the Infrastructure As Code component that will drive value on code being repeatable, auditable and efficient.

# APPROACH #
The deployment of both Application and Infrastructure will be driven by Terraform.

# INSTALLATION (MacOS)#
- Install Homebrew for MacOS by opening a terminal and typing in this command: /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
- brew install warrensbox/tap/tfswitch
- Then type in: tfswitch
- As the 'tfswitch' command enables you toselect the Terraform version to use, I highly recommend selecting version .11.14 

# Related Links #
https://planwise.jira.com/wiki/spaces/DSCI/pages/821985525/Infrastructure+as+Code